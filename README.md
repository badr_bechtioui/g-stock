# G-Stock

#### G-Stock App With Symfony 5, Docker, Nginx, MariaDB.

Online Inventory Management Software will help you to manage your product stock in manageable way.
This system is built on Symfony with proper management of users, groups, brand, stores, product, orders and reports.
You can create as many users as you want and assigned them with required modules. The system features are listed below section.

This system can be also used for small business. It is free web based inventory management software.
This system is based on the store inventory system. The products are controlled by the store.


### System Features
* Manage Users
    * Add new user detail
    * View, Update, and remove user information
* Manage Groups
    * Add new group information
    * View, Update, and remove group information
* Manage Brands
    * Add new brand data
    * View, Update, and remove brand information
* Manage Category
    * Add new category information
    * View, Update, and remove category information
* Manage Stores
    * Add new store information
    * View, Update, and remove stores information
* Manage Attributes
    * Add new attribute information
    * View, Update, and remove attributes information
* Manage Products
    * Add new product information
    * View, Update, and remove products information
* Manage Orders
    * Add new order information
    * View, Update, and remove orders information
* Reports
    * View total amount of sales represented on the graphical chart based on yearly.
* Company
    * Update the company information
    * That includes company name, address, phone, message, vat charge, service charge and more..
* Profile
    * View the logged in user information
* Setting
    * View, and Update logged in user information


# Installation guidelines:

### Installing Symfony 5 (app folder) :

`composer create-project symfony/website-skeleton app`


### Configuring the Database :

The database connection information is stored as an environment variable called DATABASE_URL.
For development, you can find and customize this inside app >> .env:

```
# to use mariadb:
DATABASE_URL="mysql://badr:123456@test_database:3306/test_db?serverVersion=mariadb-10.5.9"
```

### Main Tasks :

#### Creation of the Home Page :

`symfony make:controller`

- Created: src/Controller/HomePageController.php
- Created: templates/home_page/index.html.twig


#### Creation of the User entity & Migrate to the DB :

`symfony make:user`

- Created: src/Entity/User.php
- Created: src/Repository/UserRepository.php
- Updated: config/packages/security.yaml

`symfony make:entity`

- Updated: src/Entity/User.php
- N.B: this command is executed to add **firstname**, **lastname**, **username**, **phone** and **gender** fields to the User entity.

`symfony make:migration`

- Created: migrations/Version20210611123513.php

`symfony doctrine:migrations:migrate`

- N.B: the user table is created in the Database.


#### Creation of a registration system :

`symfony make:controller RegisterController`

- Created: src/Controller/RegisterController.php
- Created: templates/frontend/register/index.html.twig


`symfony make:form RegisterType`

- Created: src/Form/RegisterFormType.php


`composer require giggsey/libphonenumber-for-php`

- Created: app/composer.json
- Updated: app/composer.lock


`symfony make:validator Telephone`

- Created: src/Validator/Telephone.php
- Created: src/Validator/TelephoneValidator.php

#### Creation of a login system :

`symfony make:auth LoginFormAuthenticator`

- Created: src/Security/LoginFormAuthenticator.php
- Updated: config/packages/security.yaml
- Created: src/Controller/SecurityController.php
- Created: templates/frontend/security/login.html.twig


#### Creation of the Admin Dashboard :

`symfony make:controller`

- Created: src/Controller/DashboardController.php
- Created: templates/backend/dashboard/index.html.twig

#### All groups listing

  ##### Creation of the Group entity :
  
  `symfony make:entity Group`
  
  - Created: src/Entity/Group.php
  - Created: src/Repository/GroupRepository.php
  
  ##### Migrate to the DB & CRUD :
  
  `symfony make:migration`
  
  - Created: migrations/Version20210611190552.php
  
  `symfony doctrine:migrations:migrate`
  
  - N.B: the Group table is created in the Database.
  
  ##### Make Listing :
  
  `symfony make:crud`
  
  - Created: src/Controller/GroupController.php
  - Created: templates/backend/group/index.html.twig

#### Add New Group Information :

- Created: src/Form/GroupType.php
- Created: templates/backend/group/_form.html.twig
- Created: templates/backend/group/new.html.twig

#### Show a Group Information :

- Created: templates/backend/group/show.html.twig


#### Edit a Group Information :

- Created: templates/backend/group/edit.html.twig

#### Delete a Group :

- Created: templates/backend/group/show.html.twig


#### Creation of Pagination for groups listing :

`composer require knplabs/knp-paginator-bundle`

- Updated: composer.json
- Updated: composer.lock
- Updated: config/bundles.php
- Updated: symfony.lock

#### Creation of Search system for groups listing :

- Create SearchGroup class inside src/Search
- Create SearchGroupType class inside src/Form
- Adding findSearch function inside GroupRepository
- Update GroupController


#### All users listing

`symfony make:controller UserController`

- Created: src/Controller/UserController.php
- Created: templates/backend/user/index.html.twig

`symfony make:entity User`

- Updated: src/Entity/User.php
- Updated: src/Entity/Group.php
- NB: This command to add the ManyToMany relation between the User & Group Entity

`symfony make:migration`

- Created: migrations/Version20210612191600.php

`symfony doctrine:migrations:migrate`

- NB: This command add the user_group table in the DB


#### Add New User Information :

`symfony make:form UserType`

- Created: src/Form/UserType.php
  
- Creating templates/backend/user/_form.html.twig
- Creating templates/backend/user/new.html.twig
- Adding new() function inside the UserController

#### Show a User Information :

- Creating templates/backend/user/show.html.twig
- Adding show() function inside the UserController

#### Edit a User Information :

- Creating templates/backend/user/edit.html.twig
- Adding edit() function inside the UserController

#### Delete a User :

- Creating templates/backend/user/show.html.twig
- Adding delete() function inside the UserController

#### Creation of Pagination for users listing :

- Editing the index() function inside the UserController
- Editing the index.html.twig template

#### Creation of Search system for users listing :

- Create SearchUser class inside src/Search
- Create SearchUserType class inside src/Form
- Adding findSearch function inside UserRepository
- Adding a _search_form.html.twig template
- Update UserController


#### All stores listing

 ##### Creation of the Stores entity :
  
  `symfony make:entity Group`
  
  - Created: src/Entity/Stores.php
  - Created: src/Repository/StoresRepository.php
  
  ##### Migrate to the DB & CRUD :
  
  `symfony make:migration`
  
  - Created: migrations/Version20210613175016.php
  
  `symfony doctrine:migrations:migrate`
  
  - N.B: the Group table is created in the Database.
  
  ##### Make Listing :
  
  `symfony make:crud`
  
  - Created: src/Controller/StoresController.php
  - Created: templates/backend/Stores/index.html.twig

  #### Add New Store Information :
+
+`symfony make:form StoresType`
+
+- Created: src/Form/Storesype.php
+  
+- Creating templates/backend/Stores/_form.html.twig
+- Creating templates/backend/stores/new.html.twig
+- Adding new() function inside the StoresController


#### Show a store Information :

- Creating templates/backend/stores/show.html.twig
- Adding show() function inside the StoresController

#### Edit a store Information :

- Creating templates/backend/stores/edit.html.twig
- Adding edit() function inside the StoresController
- Adding in asside and stores/index.html.twig

#### Delete a store :

- Created: templates/backend/stores/show.html.twig
- Adding delete() function inside the StoresController


#### Creation of Pagination for store listing :

`composer require knplabs/knp-paginator-bundle`

- Updated: composer.json
- Updated: composer.lock
- Updated: config/bundles.php
- Updated: symfony.lock


#### Creation of Search system for store listing :


- Create SearchGroup class inside src/Search
- Create SearchGroupType class inside src/Form
- Adding findSearch function inside GroupRepository
- Update GroupController


#### All brand listing

 ##### Creation of the brand entity :
  
  `symfony make:entity Brand`
  
  - Created: src/Entity/Brand.php
  - Created: src/Repository/BrandRepository.php
  - Adding index() function inside the BrandController
  - Adding in asside and Brand/index.html.twig
  

  #### Show a brand Information :

- Creating templates/backend/brand/show.html.twig
- Adding show() function inside the brandController

	modified:   src/Controller/BrandController.php
	modified:   templates/backend/brand/index.html.twig
	create : src/Form/BrandType.php
	create : templates/backend/brand/_form.html.twig
	
#### Add New brand Information :

`symfony make:form StoresType`

- Created: src/Form/Storesype.php
- Creating templates/backend/Stores/_form.html.twig
- Creating templates/backend/stores/new.html.twig
- Adding new() function inside the StoresController

#### edite a brand Information :

    modified:   src/Controller/BrandController.php
	new file:   templates/backend/brand/edit.html.twig
	modified:   templates/backend/brand/index.html.twig
	modified:   templates/backend/brand/show.html.twig


#### Delete a brand :

    modified:   src/Controller/BrandController.php
	modified:   templates/backend/brand/index.html.twig
	Creating:   templates/backend/brand/_delete_form.html.twig

####  Creation of Pagination for brand listing :
    
    modified:   src/Controller/BrandController.php
	modified:   templates/backend/brand/_delete_form.html.twig
	modified:   templates/backend/brand/index.html.twig
	modified:   templates/backend/brand/show.html.twig


#### All Category listing

 ##### Creation of the Category entity :
  
  `symfony make:entity Category`

    modified:   templates/backend/aside.html.twig
	migrations/Version20210621174646.php
	src/Controller/CategoryController.php
	src/Entity/Category.php
	src/Repository/CategoryRepository.php
	created: templates/category/index.html.twig


  #### Show a Category Information :

- Creating templates/backend/Category/show.html.twig
- Adding show() function inside the CategoryController
	modified:   templates/backend/Category/index.html.twig
	create : src/Form/CategoryType.php
	create : templates/backend/Category/_form.html.twig
	