<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 * @ORM\Table(name="`brand`")
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="La marque est obligatoire"
     * )
     * @Assert\Length(
     *     min=3,
     *     max=15,
     *     minMessage="Le nom du marque doit contenir au moins trois caractères",
     *     maxMessage="Le nom du marque doit contenir au maximum quinze caractères"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Choice({0, 1})
     * @Assert\NotBlank(
     *     message="Le status est obligatoire"
     * )
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity=Products::class, mappedBy="brand")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }


    /**
     * @return Collection|Products[]
     */
    public function getProduit(): Collection
    {
        return $this->products;
    }

    public function addProduit(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBrand($this);
        }

        return $this;
    }

    public function removeProduit(Products $product): self
    {
        if ($this->product->removeElement($product)) {
            $product->removeBrand($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
