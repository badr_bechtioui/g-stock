<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as MyAssert;

/** @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="Il semble que vous êtes déjà inscrit avec cette adresse e-mail"
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(
     *     message="Adresse e-mail non valide"
     * )
     * @Assert\NotBlank(
     *     message="L'adresse email est obligatoire"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(
     *     message="Le mot de pass est obligatoire"
     * )
     * @Assert\NotCompromisedPassword(
     *     message="Le mot de passe n'est pas valide, il est péférable d'utiliser des nombres, des caratéers est des symboles"
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(
     *     message="Le prénom est obligatoire"
     * )
     * @Assert\Length(
     *      min=2,
     *      max=10,
     *      minMessage="Votre prénom doit contenir au moins deux caractères",
     *      maxMessage="Votre prénom doit contenir au maximum dix caractères"
     * )
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank(
     *     message="Le nom est obligatoire"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=10,
     *     minMessage="Votre nom doit contenir au moins deux caractères",
     *     maxMessage="Votre nom doit contenir au maximum dix caractères"
     * )
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(
     *     message="Le nom d'utilisateur est obligatoire"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=15,
     *     minMessage="Votre nom d'utilisateur doit contenir au moins cinq caractères",
     *     maxMessage="Votre nom d'utilisateur doit contenir au maximum quinze caractères"
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=20)
     * @MyAssert\Telephone
     */
    private $phone;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Choice({0, 1})
     * @Assert\NotBlank(
     *     message="Le gender est obligatoire"
     * )
     */
    private $gender;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, inversedBy="users")
     */
    private $groups;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(?string $password): self
    {
        if (!is_null($password)) {
            $this->password = $password;
        }

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(?int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

   
    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroups(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        $this->groups->removeElement($group);

        return $this;
    }
    public function __toString()
    {
        return $this->getFirstname();
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

   
}
