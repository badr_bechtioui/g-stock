<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use App\Validator as MyAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     message="Le nom du client est obligatoire"
     * )
     * @Assert\Length(
     *      min=3,
     *      max=50,
     *      minMessage="Le nom du client doit contenir au moins trois caractères",
     *      maxMessage="Le nom du client doit contenir au maximum cinquante caractères"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(
     *     message="L'adresse est obligatoire"
     * )
     * @Assert\Length(
     *      min=10,
     *      max=50,
     *      minMessage="L'adresse doit contenir au moins dix caractères",
     *      maxMessage="L'adresse doit contenir au maximum cinquante caractères"
     * )
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\Length(
     *     max=20,
     *     maxMessage="Le numéro de téléphone est très long!"
     * )
     * @MyAssert\Telephone
     */
    private $phone;

    /**
     * @ORM\Column(type="smallint")
     * @Assert\Choice({0, 1})
     * @Assert\NotBlank(
     *     message="Le status du client est obligatoire"
     * )
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
