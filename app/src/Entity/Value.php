<?php

namespace App\Entity;

use App\Repository\ValueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ValueRepository::class)
 * @ORM\Table(name="`value`")
 */
class Value
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Value est obligatoire"
     * )
     * @Assert\Length(
     *     max=15,
     *     maxMessage="Le nom de Value doit contenir au maximum quinze caractères"
     * )
     */

    private $name;

   

    /**
     * @ORM\ManyToMany(targetEntity=Products::class, mappedBy="value")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProduct(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {

        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addValue($this);
        }
        return $this;
    }

   

    public function removeProduct(Products $product): self
    {
        if ($this->product->removeElement($product)) {
            $product->removeValue($this);
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getName();
    }
}
