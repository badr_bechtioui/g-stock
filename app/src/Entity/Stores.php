<?php

namespace App\Entity;

use App\Repository\StoresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=StoresRepository::class)
 * @ORM\Table(name="`stores`")
 */
class Stores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     message="Le store est obligatoire"
     * )
     * @Assert\Length(
     *     min=3,
     *     max=20,
     *     minMessage="Le nom de store doit contenir au moins trois caractères",
     *     maxMessage="Le nom de store doit contenir au maximum vingt caractères"
     * )
     */
    private $name;

     /**
     * @ORM\Column(type="smallint")
     * @Assert\Choice({0, 1})
     * @Assert\NotBlank(
     *     message="Le status est obligatoire"
     * )
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity=Products::class, mappedBy="store")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setStore($this);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->product->removeElement($product)) {
            $product->removeStore($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
