<?php

namespace App\Entity;

use App\Repository\ProductsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as MyAssert;

/**
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 * @ORM\Table(name="`product`")
 */
class Products
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="float")
     */
    private $qty;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /** 
     * @ORM\Column(type="smallint")
     */
    private $availability;



    /**
     * @ORM\ManyToMany(targetEntity=Stores::class, inversedBy="products")
     */
    private $store;

    /**
     * @ORM\ManyToMany(targetEntity=Brand::class, inversedBy="produit")
     */
    private $brand;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="products")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity=Value::class, inversedBy="product")
     */
    private $value;

    /**
     * @ORM\ManyToMany(targetEntity=Size::class, inversedBy="products")
     */
    private $size;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Assert\Image(
     *     minWidth = 400,
     *     minWidthMessage = "La largeur de l'image est trop petite ({{ width }}px). La largeur minimale attendue est {{ min_width }}px",
     *     maxWidth = 1200,
     *     maxWidthMessage = "La largeur de l'image est trop grande ({{ width }}px). La largeur maximale attendue est {{ max_width }}px",
     *     minHeight = 400,
     *     minHeightMessage = "La hauteur de l'image est trop petite ({{ height }}px). La hauteur minimale attendue est {{ min_height }}px",
     *     maxHeight = 1200,
     *     maxHeightMessage = "La hauteur de l'image est trop grande ({{ height }}px). La hauteur maximale attendue est {{ max_height }}px",
     *     mimeTypes = {"image/jpeg", "image/png","image/jpg", "image/gif"},
     *     mimeTypesMessage = "Uniquement .jpeg .png .jpg et .gif Extension valide"
     * )
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=Order::class, mappedBy="product")
     */
    private $orders;

   
    public function __construct()
    {
        $this->store = new ArrayCollection();
        $this->brand = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->value = new ArrayCollection();
        $this->size = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQty(): ?float
    {
        return $this->qty;
    }

    public function setQty(float $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAvailability(): ?int
    {
        return $this->availability;
    }

    public function setAvailability(int $availability): self
    {
        $this->availability = $availability;

        return $this;
    }


    /**
     * @return Collection|Stores[]
     */
    public function getStore(): Collection
    {
        return $this->store;
    }

    public function setStore(Stores $store): self
    {
        if (!$this->store->contains($store)) {
            $this->store[] = $store;
        }
        return $this;
    }


    public function removeStore(Stores $store): self
    {
        if ($this->store->removeElement($store));

        return $this;
    }


    /**
     * @return Collection|Brand[]
     */
    public function getBrand(): Collection
    {
        return $this->brand;
    }

    public function setBrand(Brand $brand): self
    {
        if (!$this->brand->contains($brand)) {
            $this->brand[] = $brand;
        }

        return $this;
    }

    public function removeBrand(Brand $brand): self
    {
        $this->brand->removeElement($brand);

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getValue(): Collection
    {
        return $this->value;
    }

    public function addValue(Value $value): self
    {
        if (!$this->value->contains($value)) {
            $this->value[] = $value;
        }

        return $this;
    }

    public function removeValue(Value $value): self
    {
        $this->value->removeElement($value);

        return $this;
    }

    /**
     * @return Collection|Size[]
     */
    public function getSize(): Collection
    {
        return $this->size;
    }

    public function addSize(Size $size): self
    {
        if (!$this->size->contains($size)) {
            $this->size[] = $size;
        }

        return $this;
    }

    public function removeSize(Size $size): self
    {
        $this->size->removeElement($size);

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->addProduct($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            $order->removeProduct($this);
        }

        return $this;
    }

    
    public function __toString()
    {
        return (string) $this->getName()."  - ".$this->getPrice()." DH";
    }
   

   public function unserialize($serialized)
   {
    list(
        $this->id,
        $this->name,
        $this->image,
        $this->category,
        $this->price,
        
        // see section on salt below
        // $this->salt
    ) = unserialize($serialized);
   }
    

   }
