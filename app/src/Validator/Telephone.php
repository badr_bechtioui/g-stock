<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Telephone extends Constraint
{
    public $invalid_message = "Ce numéro de téléphone '{{ value }}' n'est pas valide.";
    public $empty_message = "Le numéro de téléphone est obligatoire.";
}
