<?php

namespace App\Validator;

use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TelephoneValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint Telephone */

        if (null === $value || '' === $value) {
            return $this->context
                ->buildViolation($constraint->empty_message)
                ->addViolation();
        }

        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        try {
            $phoneNumberObject = $phoneNumberUtil->parse($value, null);
            if ($phoneNumberUtil->isValidNumber($phoneNumberObject) === false) {
                return $this->context
                    ->buildViolation($constraint->invalid_message)
                    ->setParameter('{{ value }}', $value)
                    ->addViolation();
            }
        } catch (\Exception $e) {
            return $this->context
                ->buildViolation($constraint->invalid_message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
