<?php

namespace App\Controller;

use App\Entity\Value;
use App\Form\ValueType;
use App\Repository\ValueRepository;
use App\Search\SearchValue;
use App\Form\SearchValueType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/value")
 */
class ValueController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="value_index", methods={"GET"})
     */
    public function index(ValueRepository $valueRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $data = new SearchValue();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchValueType::class, $data);
        $form->handleRequest($request);

        $value = $valueRepository->findSearch($data);

        return $this->render('@backend/value/index.html.twig', [
            'value' => $value,
            'search_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new", name="value_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $value = new Value();
        $form = $this->createForm(ValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($value);
            $entityManager->flush();

            return $this->redirectToRoute('value_index');
        }

        return $this->render('@backend/value/new.html.twig', [
            'value' => $value,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/modifier-value", name="value_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Value $value): Response
    {
        $form = $this->createForm(ValueType::class, $value);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('value_index');
        }

        return $this->render('@backend/value/edit.html.twig', [
            'value' => $value,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="value_delete", methods={"POST"})
     */
    public function delete(Request $request, Value $value): Response
    {
        if ($this->isCsrfTokenValid('delete'.$value->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($value);
            $entityManager->flush();
        }

        return $this->redirectToRoute('value_index');
    }

    
}
