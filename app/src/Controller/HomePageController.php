<?php

namespace App\Controller;

use App\Repository\ProductsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(ProductsRepository $prod): Response
    {
        $ads = $prod->findAll();
        return $this->render('@frontend/home_page/index.html.twig', [
            'ads' => $ads,

        ]);
    }
}
