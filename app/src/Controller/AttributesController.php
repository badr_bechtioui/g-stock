<?php

namespace App\Controller;

use App\Entity\Attributes;
use App\Form\AttributesType;
use App\Repository\AttributesRepository;
use App\Form\SearchAttributeType;
use App\Search\SearchAttribute;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/attributes")
 */
class AttributesController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="attributes_index", methods={"GET"})
     */
    public function index(AttributesRepository $attributesRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $data = new SearchAttribute();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchAttributeType::class, $data);
        $form->handleRequest($request);

        $attributes = $attributesRepository->findSearch($data);

    
        return $this->render('@backend/attributes/index.html.twig', [
            'attributes' => $attributes,
            'search_form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/new", name="attributes_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $attribute = new Attributes();
        $form = $this->createForm(AttributesType::class, $attribute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($attribute);
            $entityManager->flush();
            

            return $this->redirectToRoute('attributes_index');
        }

        return $this->render('@backend/attributes/new.html.twig', [
            'attribute' => $attribute,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attributes_show", methods={"GET"})
     */
    public function show(Attributes $attribute): Response
    {
        return $this->render('@backend/attributes/show.html.twig', [
            'attribute' => $attribute,
        ]);
    }
    
     /**
     * @Route("/{id}", name="attributes_delete", methods={"POST"})
     */
    public function delete(Request $request, Attributes $attribute): Response
    {
        if ($this->isCsrfTokenValid('delete'.$attribute->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($attribute);
            $entityManager->flush();
        }

        return $this->redirectToRoute('attributes_index');
    }

    /**
     * @Route("/{id}/edit", name="attributes_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Attributes $attribute): Response
    {
        $form = $this->createForm(AttributesType::class, $attribute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('attributes_index');
        }

        return $this->render('@backend/attributes/edit.html.twig', [
            'attribute' => $attribute,
            'form' => $form->createView(),
        ]);
    }
}
