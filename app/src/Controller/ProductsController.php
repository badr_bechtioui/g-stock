<?php

namespace App\Controller;

use App\Entity\Products;
use App\Form\ProductsType;
use App\Repository\ProductsRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Search\SearchProduct;
use App\Form\SearchProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\File;


/**
 * @Route("/products")
 */
class ProductsController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    
    /**
     * @Route("/", name="products_index", methods={"GET"})
     */
    public function index(ProductsRepository $productsRepository, Request $request): Response
    {
        $data = new SearchProduct();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchProductType::class, $data);
        $form->handleRequest($request);

        $Product = $productsRepository->findSearch($data);

        return $this->render('@backend/products/index.html.twig', [
            
            'products' => $Product,
            'search_form' => $form->createView()
        ]);
        
    }
    
     /**
     * @Route("/new", name="products_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $product = new Products();
        $form = $this->createForm(ProductsType::class, $product);
        $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
            
            foreach ($product->getStore() as $store) {
                $store->addProduct($product);
            }

           /*
            foreach ($product->getBrand() as $brand) {
                $brand->addProduct($product);
            }
            
            foreach ($product->getCategory() as $category) {
                $category->addProduct($product);
            }
            */

            //On récupére les images transmmises
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $form->get('image')->getData();

            if ($uploadedImage) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
                $newImageName = uniqid() . '_' . $uploadedImage->getClientOriginalName();
                $uploadedImage->move(
                    $destination,
                    $newImageName);
                $product->setImage($newImageName);
            }
            
            
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            $this->addFlash('success', 'Votre Produit a été ajouté avec succès!');

            return $this->redirectToRoute('products_index');
        }

        return $this->render('@backend/products/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="products_show", methods={"GET"})
     */
    public function show(Products $product): Response
    {
        $count = count($product->getStore());
        return $this->render('@backend/products/show.html.twig', [
            'product' => $product,
            'count' => $count
        ]);
    }

    /**
     * @Route("/{id}/edit", name="products_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Products $product): Response
    {
        $form = $this->createForm(ProductsType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $form->get('image')->getData();

            if ($uploadedImage) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
                $newImageName = uniqid() . '_' . $uploadedImage->getClientOriginalName();
                $uploadedImage->move(
                    $destination,
                    $newImageName);
                $product->setImage($newImageName);
            }
            
            

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre Produit a été modifié avec succès!');

            return $this->redirectToRoute('products_index');
        }

        return $this->render('@backend/products/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

   
    /**
     * @Route("/{id}", name="products_delete", methods={"POST"})
     */
    public function delete(Request $request, Products $product): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
            $this->addFlash('success', 'La Produit a été supprimé avec succès!');
        }

        return $this->redirectToRoute('products_index');
    }

    

    
}
