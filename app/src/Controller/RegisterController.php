<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $success_message = null;

        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

             //On récupére les images transmmises
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $form->get('image')->getData();

            if ($uploadedImage) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
                $newImageName = uniqid() . '_' . $uploadedImage->getClientOriginalName();
                $uploadedImage->move(
                    $destination,
                    $newImageName);
                $user->setImage($newImageName);
            }

            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $success_message = "Votre inscription à notre application s'est déroulée avec succès !";
        }

        return $this->render('@frontend/register/index.html.twig', [
            'register_form' => $form->createView(),
            'success_message' => $success_message
        ]);
    }
}
