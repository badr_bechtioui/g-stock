<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/company")
 */
class CompanyController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="company_index", methods={"GET","POST"})
     */
    public function index(Request $request, CompanyRepository $companyRepository): Response
    {
       
        $company = $companyRepository->find(array('id' => 1));

        if (!$company) {
            $company = new Company();
            $form = $this->createForm(CompanyType::class, $company);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($company);
                $entityManager->flush();
                $this->addFlash('success', 'Votre Compagnie a éte ajouté avec succès!');



                return $this->redirectToRoute('company_index');
                
            }

            return $this->render('@backend/company/new.html.twig', [
                'company' => $company,
                'form' => $form->createView(),
               

            ]);
            return $this->redirectToRoute('company_index');
        } else {
            $form = $this->createForm(CompanyType::class, $company);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Votre Compagnie a éte modifié avec succès!');

                return $this->redirectToRoute('company_index');
            }

            return $this->render('@backend/company/edit.html.twig', [
                'company' => $company,
                'form' => $form->createView(),
               
            ]);
        }
    }
}
