<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use App\Search\SearchGroup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GroupController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/groupes", name="group_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('@backend/group/index.html.twig');
    }

    /**
     * @Route("/liste-de-groupes", name="fetchGroups", methods={"GET"})
     */
    public function fetchGroups(GroupRepository $groupRepository, Request $request): JsonResponse
    {
        $data = new SearchGroup();
        $data->q = $request->get('q');
        

        $limit = 10;

        $page = (int)$request->get('page', 1);

        $total = $groupRepository->findSearchTotal($data);
        $groupes = $groupRepository->findSearchPaginated($data, $page, $limit);

        return $this->json([
            'groups' => $groupes,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);

        
    }
    /**
     * @Route("/nouveau-groupes", name="group_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $groupe = new Group();
        $groupe->setName($request->get('name'));
        $groupe->setDescription($request->get('description'));

        $errors = $validator->validate($groupe);

        $errorMessages = array();

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json([
                'errtest' => 400,
                'errors' => $errorMessages,
            ]);
        } else {
            $this->entityManager->persist($groupe);
            $this->entityManager->flush();

            return $this->json([
                'errtest' => 200,
                'message' => 'La groupe a bien été ajouté',
            ]);
        }
    }

    /**
     * @Route("/ajouter-groupes", name="group_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $groupes = new Group();
        $form = $this->createForm(GroupType::class, $groupes);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($groupes);
            $this->entityManager->flush();

            return $this->redirectToRoute('group_index');
        }

        return $this->render('@backend/group/new.html.twig', [
            'groupe' => $groupes,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/afficher-groupes/{id}", name="group_show", methods={"GET"})
     */
    public function show(GroupRepository $groupeRepository, int $id): JsonResponse
    {
        $groupe = $groupeRepository->find($id);

        if ($groupe) {
            return $this->json([
                'errtest' => 200,
                'groupe' => $groupe,
            ]);
        } else {
            return $this->json([
                'errtest' => 404,
                'message' => 'groupe introuvable'
            ]);
        }
    }
    /**
     * @Route("/modifier-groupes/{id}", name="group_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Group $groupe, ValidatorInterface $validator): JsonResponse
    {
        if ($groupe) {
            $groupe->setName($request->get('name'));
            $groupe->setDescription($request->get('description'));


            $errors = $validator->validate($groupe);

            $errorMessages = [];

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                return $this->json([
                    'statusErr' => 400,
                    'errors' => $errorMessages,
                ]);
            } else {
                $this->entityManager->flush();

                return $this->json([
                    'statusErr' => 200,
                    'message' => 'Le groupe a bien été modifié',
                ]);
            }
        } else {
            return $this->json([
                'statusErr' => 404,
                'message' => "Erreur! le groupe n'a pas été trouvié"
            ]);
        }
    }

    /**
     * @Route("/supprimer-groupes/{id}", name="group_delete", methods={"POST"})
     */
    public function delete(Group $groupe): JsonResponse
    {
        $this->entityManager->remove($groupe);
        $this->entityManager->flush();

        return $this->json([
            'statusErr' => 200,
            'message' => 'Le groupe a été bien supprimé!',
        ]);
    }
}
