<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\SearchUserType;
use App\Search\SearchUser;



/**
 * @Route("/utilisateurs")
 */
class UserController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, Request $request): Response
    {
        $data = new SearchUser();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchUserType::class, $data);
        $form->handleRequest($request);

        $users = $userRepository->findSearch($data);

        return $this->render('@backend/user/index.html.twig', [
            'users' => $users,
            'search_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/nouveau-utilisateur", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($user->getGroups() as $group) {
                $group->addUser($user);
            }

            //On récupére les images transmmises
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $form->get('image')->getData();

            if ($uploadedImage) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
                $newImageName = uniqid() . '_' . $uploadedImage->getClientOriginalName();
                $uploadedImage->move(
                    $destination,
                    $newImageName);
                $user->setImage($newImageName);
            }

            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'Votre User a été ajouté avec succès!');

            return $this->redirectToRoute('user_index');
        }

        return $this->render('@backend/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/afficher-utilisateur/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $count = count($user->getGroups());

        return $this->render('@backend/user/show.html.twig', [
            'user' => $user,
            'count' => $count
        ]);
    }

    /**
     * @Route("/modifier-utilisateur/{id}", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserRepository $userRepository, UserPasswordEncoderInterface $encoder): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {
            
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            
             //On récupére les images transmmises
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $form->get('image')->getData();

            if ($uploadedImage) {
                $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
                $newImageName = uniqid() . '_' . $uploadedImage->getClientOriginalName();
                $uploadedImage->move(
                    $destination,
                    $newImageName);
                $user->setImage($newImageName);
            }

            $this->entityManager->flush();
            $this->addFlash('success', 'Votre User a été modifié avec succès!');

            return $this->redirectToRoute('user_index');
        }
        

           

        return $this->render('@backend/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
            $this->addFlash('success', 'Votre User a été supprimé avec succès!');
        }

        return $this->redirectToRoute('user_index');
    }
}
