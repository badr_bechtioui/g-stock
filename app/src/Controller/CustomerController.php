<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Form\CustomerType;
use App\Repository\CustomerRepository;
use App\Search\SearchCustomer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CustomerController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/clients", name="customer_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('@backend/customer/index.html.twig');
    }

    /**
     * @Route("/liste-de-clients", name="fetchCustomers", methods={"GET"})
     */
    public function fetchCustomers(CustomerRepository $customerRepository, Request $request): JsonResponse
    {
        $data = new SearchCustomer();
        $data->q = $request->get('q');
        $data->status = $request->get('status');

        $limit = 10;

        $page = (int)$request->get('page', 1);

        $total = $customerRepository->findSearchTotal($data);
        $customers = $customerRepository->findSearchPaginated($data, $page, $limit);

        return $this->json([
            'customers' => $customers,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/nouveau-client", name="customer_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $customer = new Customer();
        $customer->setName($request->get('name'));
        $customer->setAddress($request->get('address'));
        $customer->setPhone($request->get('phone'));
        $customer->setStatus($request->get('status'));

        $errors = $validator->validate($customer);

        $errorMessages = array();

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json([
                'status' => 400,
                'errors' => $errorMessages,
            ]);
        } else {
            $this->entityManager->persist($customer);
            $this->entityManager->flush();

            return $this->json([
                'status' => 200,
                'message' => 'Le client a bien été ajouté',
            ]);
        }
    }

    /**
     * @Route("/ajouter-client", name="customer_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($customer);
            $this->entityManager->flush();

            return $this->redirectToRoute('customer_index');
        }

        return $this->render('@backend/customer/new.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/afficher-client/{id}", name="customer_show", methods={"GET"})
     */
    public function show(CustomerRepository $customerRepository, int $id): JsonResponse
    {
        $customer = $customerRepository->find($id);

        if ($customer) {
            return $this->json([
                'status' => 200,
                'customer' => $customer,
            ]);
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'Client introuvable'
            ]);
        }
    }

    /**
     * @Route("/modifier-client/{id}", name="customer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Customer $customer, ValidatorInterface $validator): JsonResponse
    {
        if ($customer) {
            $customer->setName($request->get('name'));
            $customer->setAddress($request->get('address'));
            $customer->setPhone($request->get('phone'));
            $customer->setStatus($request->get('status'));

            $errors = $validator->validate($customer);

            $errorMessages = [];

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                return $this->json([
                    'status' => 400,
                    'errors' => $errorMessages,
                ]);
            } else {
                $this->entityManager->flush();

                return $this->json([
                    'status' => 200,
                    'message' => 'Le client a bien été modifié',
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => "Erreur! le client n'a pas été trouvé"
            ]);
        }
    }

    /**
     * @Route("/supprimer-client/{id}", name="customer_delete", methods={"POST"})
     */
    public function delete(Customer $customer): JsonResponse
    {
        $this->entityManager->remove($customer);
        $this->entityManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'Le client a été bien supprimé!',
        ]);
    }

}
