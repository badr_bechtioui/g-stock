<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\BrandRepository;
use App\Repository\ProductsRepository;
use App\Repository\StoresRepository;
use App\Repository\CategoryRepository;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class DashboardController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/tableau-de-bord", name="dashboard")
     */
    public function index(ProductsRepository $productsRepository, BrandRepository $brandRepository,  StoresRepository $storesRepository, CategoryRepository $categoryRepository, OrderRepository $OrderRepository ): Response
    {
        $statiProduct = count($productsRepository->findAll());
        $statiBrand = count($brandRepository->findAll());
        $statiStore = count($storesRepository->findAll());
        $statiCategory = count($categoryRepository->findAll());
        $statiOrder = count($categoryRepository->findAll());

         
        $prodName = [];
        $prodPrice = [];

        foreach($productsRepository as $products){
            $prodName [] = $products->getName();
            $prodPrice [] = $products->getPrice();
        }



        return $this->render('@backend/dashboard/index.html.twig', [
            'statiProduct'  => $statiProduct,
            'statiBrand'    => $statiBrand,
            'statiStore'    => $statiStore,
            'statiCategory' => $statiCategory,
            'statiOrder'    => $statiOrder,          
            'prodName' => json_encode($prodName),
            'prodPrice' => json_encode($prodPrice),

        ]);
    }

    
}
