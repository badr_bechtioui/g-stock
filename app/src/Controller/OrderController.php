<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Products;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use App\Repository\ProductsRepository;
use App\Entity\Company;
use App\Search\SearchOrder;
use App\Form\SearchOrderType;
use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="order_index", methods={"GET"})
     */
    public function index(OrderRepository $orderRepository, Request $request): Response
    {
        $data = new SearchOrder();
        $data->page = $request->get('page', 1);

        $form = $this->createForm(SearchOrderType::class, $data);
        $form->handleRequest($request);

        $Order = $orderRepository->findSearch($data);

        return $this->render('@backend/order/index.html.twig', [
            'orders' => $Order,
            'search_form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/new", name="order_new", methods={"GET","POST"})
     */
    public function new(Request $request, CompanyRepository $companyRepository, ProductsRepository $productsRepository): Response
    {
        $order = new Order();
     
        $form = $this->createForm(OrderType::class, $order);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($order->getProduct() as $product) {
                $product->addOrder($order);
            }
            
            $price = $form->get('price')->getData();
            $rate = $form->get('rate')->getData();
            $qty = $form->get('qty')->getData();
            $order->setAmount($rate * $qty);
            

            //$price = $form->get('price')->getData();
            //$product->setPrice($price );

           $bill = rand(1, 10000);
           $order->setBill('FACGS-'.$bill);

           $date = date("Y-m-d H:i:s");
           $order->setDate($date);
           

            $this->entityManager->persist($order);
            
            $this->entityManager->flush();

            $this->addFlash('success', 'Votre Commande a été ajouté avec succès!');

            return $this->redirectToRoute('order_index');
        }

        
        return $this->render('@backend/order/new.html.twig', [
            'order' => $order,
          
            'form'  => $form->createView(),

            'company' => $companyRepository->findAll(),

            'product' => $productsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_show", methods={"GET"})
     */
    public function show(Order $order, companyRepository $companyRepository): Response
    {

        return $this->render('@backend/order/show.html.twig', [
            'order' => $order,
            'company' => $companyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="order_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'Votre commande a été modifié avec succès!');

            return $this->redirectToRoute('order_index');
        }


        return $this->render('@backend/order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete", methods={"POST"})
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete' . $order->getId(), $request->request->get('_token'))) {
            $this->entityManager->remove($order);
            $this->entityManager->flush();

            $this->addFlash('success', 'Votre commande a été supremer avec succès!');
        }

        return $this->redirectToRoute('order_index');
    }

    /**
     * @Route("/{id}/print", name="order_print", methods={"GET"})
     */
    public function print(Order $order): Response
    {

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);


        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('@backend/order/showPrint.html .twig', [
            'order' => $order,

        ]);


        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (inline view)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]);
        return $this->redirectToRoute('order_show');
    }
}
