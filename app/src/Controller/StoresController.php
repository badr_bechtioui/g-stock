<?php

namespace App\Controller;

use App\Entity\Stores;
use App\Form\StoresType;
use App\Repository\StoresRepository;
use App\Search\SearchStore;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class StoresController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * @Route("/magasin", name="stores_index", methods={"GET"})
     */
    public function index(): Response

    {   
        return $this->render('@backend/stores/index.html.twig');
    }

    /**
     * @Route("/liste-de-magasin", name="fetchStores", methods={"GET"})
     */
    public function fetchStores(StoresRepository $storeRepository, Request $request): JsonResponse
    {
        $data = new SearchStore();
        $data->q = $request->get('q');
        $data->status = $request->get('status');

        $limit = 10;

        $page = (int)$request->get('page', 1);

        $total = $storeRepository->findSearchTotal($data);
        $stores = $storeRepository->findSearchPaginated($data, $page, $limit);

        return $this->json([
            'stores' => $stores,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/nouveau-magasin", name="stores_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $store = new Stores();
        $store->setName($request->get('name'));
        $store->setStatus($request->get('status'));

        $errors = $validator->validate($store);

        $errorMessages = array();

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json([
                'status' => 400,
                'errors' => $errorMessages,
            ]);
        } else {
            $this->entityManager->persist($store);
            $this->entityManager->flush();

            return $this->json([
                'status' => 200,
                'message' => 'La magasin a bien été ajouté',
            ]);
        }
    }

    /**
     * @Route("/ajouter-magasin", name="store_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $store = new Stores();
        $form = $this->createForm(StoresType::class, $store);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($store);
            $this->entityManager->flush();

            return $this->redirectToRoute('stores_index');
        }

        return $this->render('@backend/store/new.html.twig', [
            'store' => $store,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/afficher-magasin/{id}", name="stores_show", methods={"GET"})
     */
    public function show(StoresRepository $storeRepository, int $id): JsonResponse
    {
        $store = $storeRepository->find($id);

        if ($store) {
            return $this->json([
                'status' => 200,
                'store' => $store,
            ]);
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'store introuvable'
            ]);
        }
    }

    /**
     * @Route("/modifier-magasin/{id}", name="store_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Stores $store, ValidatorInterface $validator): JsonResponse
    {
        if ($store) {
            $store->setName($request->get('name'));
            $store->setStatus($request->get('status'));

            $errors = $validator->validate($store);

            $errorMessages = [];

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                return $this->json([
                    'status' => 400,
                    'errors' => $errorMessages,
                ]);
            } else {
                $this->entityManager->flush();

                return $this->json([
                    'status' => 200,
                    'message' => 'La magasin a bien été modifié',
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => "Erreur! la magasin n'a pas été trouvé"
            ]);
        }
    }

    /**
     * @Route("/supprimer-magasin/{id}", name="store_delete", methods={"POST"})
     */
    public function delete(Stores $store): JsonResponse
    {
        $this->entityManager->remove($store);
        $this->entityManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'La magasin a été bien supprimé!',
        ]);
    }

}
