<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Search\SearchCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class CategoryController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/category", name="category_index", methods={"GET"})
     */

    public function index(): Response
    {

        return $this->render('@backend/category/index.html.twig');
    }
    
    /**
     * @Route("/liste-de-category", name="fetchCategorys", methods={"GET"})
     */
    public function fetchCategorys(CategoryRepository $categoryRepository, Request $request): JsonResponse
    {
        $data = new SearchCategory();
        $data->q = $request->get('q');
        $data->status = $request->get('status');

        $limit = 10;

        $page = (int)$request->get('page', 1);

        $total = $categoryRepository->findSearchTotal($data);
        $categorys = $categoryRepository->findSearchPaginated($data, $page, $limit);

        return $this->json([
            'category' => $categorys,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/nouveau-categories", name="category_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $category = new Category();
        $category->setName($request->get('name'));
        
        $category->setStatus($request->get('status'));

        $errors = $validator->validate($category);

        $errorMessages = array();

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json([
                'status' => 400,
                'errors' => $errorMessages,
            ]);
        } else {
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            return $this->json([
                'status' => 200,
                'message' => 'La categorie a bien été ajouté',
            ]);
        }
    }

    /**
     * @Route("/ajouter-category", name="category_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('@backend/category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/afficher-category/{id}", name="category_show", methods={"GET"})
     */

    public function show(CategoryRepository $categoryRepository, int $id): JsonResponse
    {
        $category = $categoryRepository->find($id);

        if ($category) {
            return $this->json([
                'status' => 200,
                'category' => $category,
            ]);
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'Categorie introuvable'
            ]);
        }
    }

    
    /**
     * @Route("/modifier-category/{id}", name="category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Category $category, ValidatorInterface $validator): JsonResponse
    {
        if ($category) {
            $category->setName($request->get('name'));
            
            $category->setStatus($request->get('status'));

            $errors = $validator->validate($category);

            $errorMessages = [];

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                return $this->json([
                    'status' => 400,
                    'errors' => $errorMessages,
                ]);
            } else {
                $this->entityManager->flush();

                return $this->json([
                    'status' => 200,
                    'message' => 'La category a bien été modifié',
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => "Erreur! la category n'a pas été trouvé"
            ]);
        }
    }

    /**
     * @Route("/supprimer-category/{id}", name="category_delete", methods={"POST"})
     */
    public function delete(Category $category): JsonResponse
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'La category a été bien supprimé!',
        ]);
    }
}
