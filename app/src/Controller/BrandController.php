<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Form\BrandType;
use App\Repository\BrandRepository;
use App\Search\SearchBrand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class BrandController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/marque", name="brand_index", methods={"GET"})
     */
    public function index(): Response
    {
       

        return $this->render('@backend/brand/index.html.twig');
    }
        

    /**
     * @Route("/liste-de-marque", name="fetchBrands", methods={"GET"})
     */
   
    public function fetchBrands(BrandRepository $brandRepository, Request $request): JsonResponse
    {
        $data = new SearchBrand();
        $data->q = $request->get('q');
        $data->status = $request->get('status');

        $limit = 10;

        $page = (int)$request->get('page', 1);

        $total = $brandRepository->findSearchTotal($data);
        $brands = $brandRepository->findSearchPaginated($data, $page, $limit);

        return $this->json([
            'brands' => $brands,
            'total' => $total,
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/nouveau-marque", name="brand_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $brand = new brand();
        $brand->setName($request->get('name'));
       
        $brand->setStatus($request->get('status'));

        $errors = $validator->validate($brand);

        $errorMessages = array();

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }
            return $this->json([
                'status' => 400,
                'errors' => $errorMessages,
            ]);
        } else {
            $this->entityManager->persist($brand);
            $this->entityManager->flush();

            return $this->json([
                'status' => 200,
                'message' => 'La marque a bien été ajouté',
            ]);
        }
    }

    /**
     * @Route("/ajouter-marque", name="brand_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $brand = new Brand();
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($brand);
            $this->entityManager->flush();

            return $this->redirectToRoute('brand_index');
        }

        return $this->render('@backend/brand/new.html.twig', [
            'brand' => $brand,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/afficher-marque/{id}", name="brand_show", methods={"GET"})
     */
    public function show(BrandRepository $brandRepository, int $id): JsonResponse
    {
        $brand = $brandRepository->find($id);

        if ($brand) {
            return $this->json([
                'status' => 200,
                'brand' => $brand,
            ]);
        } else {
            return $this->json([
                'status' => 404,
                'message' => 'Marque introuvable'
            ]);
        }
    }

    /**
     * @Route("/modifier-marque/{id}", name="brand_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Brand $brand, ValidatorInterface $validator): JsonResponse
    {
        if ($brand) {
            $brand->setName($request->get('name'));
            $brand->setStatus($request->get('status'));

            $errors = $validator->validate($brand);

            $errorMessages = [];

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorMessages[] = $error->getMessage();
                }

                return $this->json([
                    'status' => 400,
                    'errors' => $errorMessages,
                ]);
            } else {
                $this->entityManager->flush();

                return $this->json([
                    'status' => 200,
                    'message' => 'La marque a bien été modifié',
                ]);
            }
        } else {
            return $this->json([
                'status' => 404,
                'message' => "Erreur! la marque n'a pas été trouvé"
            ]);
        }
    }

    /**
     * @Route("/supprimer-marque/{id}", name="brand_delete", methods={"POST"})
     */
    public function delete(Brand $brand): JsonResponse
    {
        $this->entityManager->remove($brand);
        $this->entityManager->flush();

        return $this->json([
            'status' => 200,
            'message' => 'La marque a été bien supprimé!',
        ]);
    }

}
