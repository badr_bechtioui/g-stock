<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;


class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Type\TextType::class, [
                'label' => 'name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Nom du Client'
                ]
            ])
            ->add('charge', Type\IntegerType::class, [
                'label' => 'TVA',
                'required' => true,
                'attr' => [
                    'placeholder' => 'prix'
                ]
            ])
            ->add('address', Type\TextType::class, [
                'label' => 'adress',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Adresse du Client'
                ]
            ])
            ->add('phone', Type\TelType::class, [
                'label' => 'Téléphone',
                'required' => true,
                'attr' => [
                    'placeholder' => 'numéro de téléphone'
                ]
            ])
            ->add('country', Type\CountryType::class, [
                'label' => 'country',
                'required' => true,
                'attr' => [
                    'placeholder' => 'country du Client'
                ]
            ])
            ->add('currency', Type\CurrencyType::class, [
                'label' => 'currency',
                'required' => true,
                'attr' => [
                    'placeholder' => 'country du Client'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
