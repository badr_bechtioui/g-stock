<?php

namespace App\Form;

use App\Search\SearchAttribute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchAttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', Type\TextType::class, [
                'label' => false,
                'required' => false
            ])
            
            ->add('status', Type\ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices'  => [
                    ' EnStock' => 1,
                    ' Epuisé' => 0
                ],
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Status...',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchAttribute::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
