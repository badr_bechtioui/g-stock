<?php

namespace App\Form;

use App\Entity\Order;
use App\Entity\Products;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // ->add('company')
            ->add('product')

            ->add('name', Type\TextType::class, [
                'label' => 'name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Nom du Client'
                ]
            ])
            ->add('adress', Type\TextType::class, [
                'label' => 'adress',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Adresse du Client'
                ]
            ])
            
            ->add('phone', Type\TelType::class, [
                'label' => 'Téléphone',
                'required' => true,
                'attr' => [
                    'placeholder' => 'numéro de téléphone'
                ]
            ])
           /*
            ->add('date', Type\DateType::class, [
                'label' => 'Date de la commande',
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker form-control'],
                'format' => 'dd/mm/yy',
            ])
            */

            ->add('qty', Type\IntegerType::class, [
                'label' => 'Quantitie',
                'required' => true,
                'attr' => [
                    'placeholder' => ' '
                ]
            ])
            // ->add('bill', Type\IntegerType::class, [
            //     'label' => 'N_bill',
            //     'required' => true,
            //     'attr' => [
            //         'placeholder' => 'n°bill'
            //     ]
            // ])


            ->add('amount', Type\IntegerType::class, [
                'label' => 'Montant',
                'required' => true,
                'disabled'=> false,
                'attr' => [
                    'placeholder' => 'montante'
                ]
            ])
            ->add('rate', Type\TextType::class, [
                'label' => 'Prix',
                'required' => true,
                'attr' => [
                    'placeholder' => 'DH'
                ]
            ])
            ->add('grd_amnt', Type\IntegerType::class, [
                'label' => 'Grand Montant',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Grand Montant'
                ]
            ])
            ->add('tva', Type\TextType::class, [
                'label' => 'TVA',
                'required' => true,
                
            ])


            ->add('discount', Type\IntegerType::class, [
                'label' => 'Remise',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Remise'
                ]
            ])
            ->add('net_amnt', Type\IntegerType::class, [
                'label' => 'Montant Net',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Montant Net'
                ]
            ])
            ->add('status', Type\ChoiceType::class, [
                'label' => 'Payement',
                'required' => true,
                'choices' => [
                    'payé' => 1,
                    'impayé' => 0
                ],
                'expanded' => true,
                'multiple' => false
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
