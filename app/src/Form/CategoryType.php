<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', Type\TextType::class, [
            'label' => "Nom de la categorie",
            'required' => true
        ])
        ->add('status', Type\ChoiceType::class, [
            'label' => 'Status',
            'required' => true,
            'choices' => [
                ' En Stock' => 1,
                
                ' Epuisé' => 0
            ],
            'expanded' => true,
            'multiple' => false
        ])
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
