<?php

namespace App\Form;

use App\Entity\Products;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', Type\FileType::class, 
            ['label' => "Image de prouduit", 
            'data_class' => null,
            'required' => true, 
            'multiple' => false, 
            'attr' => ['accept' => 'image/*']])
            ->add('store')
            ->add('brand')
            ->add('category')
            ->add('value')
            ->add('size')
            ->add('sku', Type\TextType::class, [
                'label' => 'sku',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Merci de saisir le code de l Unité de Gestion de Stock'
                ]
            ])
            ->add('name', Type\TextType::class, [
                'label' => 'name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Merci de saisir le nom du produit'
                ]
            ])
            ->add('price', Type\MoneyType::class, [
                'label' => 'prix',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Merci de saisir le prix'
                ]
            ])
            ->add('qty', Type\TextType::class, [
                'label' => 'quantitie',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Merci de saisir la quantitie'
                ]
            ])
            ->add('description', Type\TextType::class, [
                'label' => 'description',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Merci de saisir la description'
                ]
            ])
            ->add('availability', Type\ChoiceType::class, [
                'label' => 'Indisponibilite',
                'required' => true,
                'choices' => [
                    'disponible' => 1,
                    'indisponible' => 0
                ],
                'expanded' => true,
                'multiple' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Products::class,
        ]);
    }
}
