<?php

namespace App\Form;

use App\Search\SearchProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', Type\TextType::class, [
                'label' => false,
                'required' => false
                ])
                ->add('availability', Type\ChoiceType::class, [
                    'label' => false,
                    'required' => false,
                    'choices' => [
                        'OUI' => 1,
                        'NON' => 0
                    ],
                    'expanded' => false,
                    'multiple' => false,
                    'placeholder' => 'Indisponibilite...',
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchProduct::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }

}