<?php

namespace App\Search;

class SearchProduct
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';

    /**
     * @var int
     */
    public $availability;
}
