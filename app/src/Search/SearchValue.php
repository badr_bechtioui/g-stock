<?php

namespace App\Search;

class SearchValue
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';
}