<?php

namespace App\Search;

class SearchGroup
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';
}