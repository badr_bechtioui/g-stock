<?php

namespace App\Search;

class SearchCustomer
{
    /**
     * @var string
     */
    public $q = '';

    /**
     * @var int
     */
    public $status;
}