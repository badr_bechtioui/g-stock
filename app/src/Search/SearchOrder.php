<?php

namespace App\Search;

class SearchOrder
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';
    
    /**
     * @var int
     */
    public $status;
}