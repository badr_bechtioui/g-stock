<?php

namespace App\Search;

class SearchStore
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';

    /**
     * @var int
     */
    public $status;
}