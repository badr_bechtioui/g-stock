<?php

namespace App\Search;

class SearchCategory
{
    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $q = '';

     /**
     * @var int
     */
    public $status;
}