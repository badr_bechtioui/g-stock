<?php

namespace App\Repository;

use App\Entity\Category;
use App\Search\SearchCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Category::class);
        
    }

     /**
     * Find list a category by a search form
     * @param SearchCategory $search
     * @return int|mixed|string
     */
    public function  findSearchTotal(SearchCategory $search)
    {
        $total = 0;
        
        $query = $this
            ->createQueryBuilder('c')
            ->select('count(c.id)');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }

        try {
            $total = $query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            // Silent exception.
        }
        return $total;
    }
    
    /**
     * @param SearchCategory $search
     * @param $page
     * @param $limit
     * @return int|mixed|string
     */
    public function findSearchPaginated(SearchCategory $search, $page, $limit)
    {
        $query = $this
            ->createQueryBuilder('c')
            ->select('c');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
               
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }
        $query = $query
            ->orderBy('c.id', 'DESC')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit);
        
        return $query->getQuery()->getResult();
    }

}