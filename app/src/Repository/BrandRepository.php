<?php

namespace App\Repository;

use App\Entity\Brand;
use App\Search\SearchBrand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Brand|null find($id, $lockMode = null, $lockVersion = null)
 * @method Brand|null findOneBy(array $criteria, array $orderBy = null)
 * @method Brand[]    findAll()
 * @method Brand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Brand::class);
        
    }

     /**
     * Find list a Brand by a search form
     * @param SearchBrand $search
     * @return int|mixed|string
     */
    public function findSearchTotal(Searchbrand $search)
    {
        $total = 0;

        $query = $this
             ->createQueryBuilder('c')
            ->select('count(c.id)');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }

        try {
            $total = $query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            // Silent exception.
        }
        return $total;
    }
    
    /**
     * @param SearchBrand $search
     * @param $page
     * @param $limit
     * @return int|mixed|string
     */
    public function findSearchPaginated(SearchBrand $search, $page, $limit)
    {
        $query = $this
            ->createQueryBuilder('c')
            ->select('c');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
               
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }
        $query = $query
            ->orderBy('c.id', 'DESC')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit);
        
        return $query->getQuery()->getResult();
    }

}