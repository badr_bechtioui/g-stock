<?php

namespace App\Repository;

use App\Entity\Group;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Search\SearchGroup;

/**
 * @method Group|null find($id, $lockMode = null, $lockVersion = null)
 * @method Group|null findOneBy(array $criteria, array $orderBy = null)
 * @method Group[]    findAll()
 * @method Group[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Group::class);
    }

    /**
     * @param SearchGroup $search
     * @return int|mixed|string
     */
    public function findSearchTotal(SearchGroup $search)
    {
        $total = 0;
        
        $query = $this
            ->createQueryBuilder('g')
            ->select('count(g.id)');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('g.name LIKE :q')
                ->orWhere('g.description LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        // if (isset($search->status)) {
        //     $query = $query
        //         ->andWhere('g.status = :status')
        //         ->setParameter('status', $search->status);
        // }

        try {
            $total = $query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            // Silent exception.
        }
        return $total;
    }
    
    /**
     * @param SearchGroup $search
     * @param $page
     * @param $limit
     * @return int|mixed|string
     */
    public function findSearchPaginated(SearchGroup $search, $page, $limit)
    {
        $query = $this
            ->createQueryBuilder('g')
            ->select('g');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('g.name LIKE :q')
                ->orWhere('g.description LIKE :q')                
                ->setParameter('q', "%{$search->q}%");
        }

        // if (isset($search->status)) {
        //     $query = $query
        //         ->andWhere('g.status = :status')
        //         ->setParameter('status', $search->status);
        // }
        
        $query = $query
            ->orderBy('g.id', 'DESC')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit);
        
        return $query->getQuery()->getResult();
    }

}

