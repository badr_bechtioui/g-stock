<?php

namespace App\Repository;

use App\Entity\Order;
use App\Search\SearchOrder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
     /**
     * @var PaginatorInterface
     */
    private $paginator;

     public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Order::class);
        $this->paginator = $paginator;
    }

    /**
     * Find list a Order by a search form
     * @param SearchSOrder $search
     * @return PaginationInterface
     */
    public function findSearch(SearchOrder $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('s')
            ->select('s');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('s.name LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        if (isset($search->status)) {
            $query = $query
                ->andWhere('s.status = :status')
                ->setParameter('status', $search->status);
        }
        return $this->paginator->paginate(
            $query,
            $search->page,
            9
        );
    }
}
