<?php

namespace App\Repository;

use App\Entity\Products;
use App\Search\SearchProduct;
use App\Form\SearchProductType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Products|null find($id, $lockMode = null, $lockVersion = null)
 * @method Products|null findOneBy(array $criteria, array $orderBy = null)
 * @method Products[]    findAll()
 * @method Products[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)

    {
        parent::__construct($registry, Products::class);
        $this->paginator = $paginator;
    }

    

    /**
     * Find list a Product by a search form
     * @param SearchProduct $search
     * @return PaginationInterface
     */
    public function findSearch(SearchProduct $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('u');

        if (!empty($search->q)) {
            $query = $query
                ->Where('u.name LIKE :q')
                ->orWhere('u.description LIKE :q')
                ->orWhere('u.sku LIKE :q')
                ->orWhere('u.price LIKE :q')
                ->orWhere('u.qty LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        if (isset($search->availability)) {
            $query = $query
                ->andWhere('u.availability = :availability')
                ->setParameter('availability', $search->availability);
        }
        return $this->paginator->paginate(
            $query,
            $search->page,
            8
        );
    }
}
