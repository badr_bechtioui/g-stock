<?php

namespace App\Repository;

use App\Entity\User;
use App\Search\SearchUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, User::class);
        $this->paginator = $paginator;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * Find list a user by a search form
     * @param SearchUser $search
     * @return PaginationInterface
     */
    public function findSearch(SearchUser $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('u');

        if (!empty($search->q)) {
            $query = $query
                ->Where('u.firstname LIKE :q')
                ->orWhere('u.lastname LIKE :q')
                ->orWhere('u.username LIKE :q')
                ->orWhere('u.phone LIKE :q')
                ->orWhere('u.gender LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        if (isset($search->gender)) {
            $query = $query
                ->andWhere('u.gender = :gender')
                ->setParameter('gender', $search->gender);
        }
        return $this->paginator->paginate(
            $query,
            $search->page,
            5
        );
    }
}
