<?php

namespace App\Repository;

use App\Entity\Value;
use App\Search\SearchValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Value|null find($id, $lockMode = null, $lockVersion = null)
 * @method Value|null findOneBy(array $criteria, array $orderBy = null)
 * @method Value[]    findAll()
 * @method Value[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValueRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Value::class);
        $this->paginator = $paginator;
    }

    /**
     * Find list a value by a search form
     * @param SearchValue $search
     * @return PaginationInterface
     */
    public function findSearch(SearchValue $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('g')
            ->select('g');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('g.name LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        return $this->paginator->paginate(
            $query,
            $search->page,
            9
        );
    }
}
