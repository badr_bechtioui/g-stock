<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Search\SearchCustomer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
     * @param SearchCustomer $search
     * @return int|mixed|string
     */
    public function findSearchTotal(SearchCustomer $search)
    {
        $total = 0;
        
        $query = $this
            ->createQueryBuilder('c')
            ->select('count(c.id)');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                ->orWhere('c.address LIKE :q')
                ->orWhere('c.phone LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }

        try {
            $total = $query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            // Silent exception.
        }
        return $total;
    }
    
    /**
     * @param SearchCustomer $search
     * @param $page
     * @param $limit
     * @return int|mixed|string
     */
    public function findSearchPaginated(SearchCustomer $search, $page, $limit)
    {
        $query = $this
            ->createQueryBuilder('c')
            ->select('c');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                ->orWhere('c.address LIKE :q')
                ->orWhere('c.phone LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }
        $query = $query
            ->orderBy('c.id', 'DESC')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit);
        
        return $query->getQuery()->getResult();
    }

}
