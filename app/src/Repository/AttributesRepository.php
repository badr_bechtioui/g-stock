<?php

namespace App\Repository;

use App\Entity\Attributes;
use App\Search\SearchAttribute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;


/**
 * @method Attributes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Attributes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Attributes[]    findAll()
 * @method Attributes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttributesRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Attributes::class);
        $this->paginator = $paginator;
    }
     /**
     * Find list a Attribute by a search form
     * @param SearchAttribute $search
     * @return PaginationInterface
     */
    public function findSearch(SearchAttribute $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('s')
            ->select('s');

        if (!empty($search->q)) {
            $query = $query
                ->andWhere('s.name LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }
        if (isset($search->status)) {
            $query = $query
                ->andWhere('s.status = :status')
                ->setParameter('status', $search->status);
        }
        if (isset($search->color)) {
            $query = $query
                ->andWhere('s.color = :color')
                ->setParameter('color', $search->color);
        }
        return $this->paginator->paginate(
            $query,
            $search->page,
            9
        );
    }
}
