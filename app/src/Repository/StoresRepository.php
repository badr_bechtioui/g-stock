<?php

namespace App\Repository;

use App\Entity\Stores;
use App\Search\SearchStore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method Stores|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stores|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stores[]    findAll()
 * @method Stores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StoresRepository extends ServiceEntityRepository
{
     public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stores::class);
       
    }

    /**
     * @param SearchStore $search
     * @return int|mixed|string
     */
    public function findSearchTotal(SearchStore $search)
    {
        $total = 0;
        
        $query = $this
            ->createQueryBuilder('c')
            ->select('count(c.id)');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }

        try {
            $total = $query->getQuery()->getSingleScalarResult();
        } catch (\Exception $e) {
            // Silent exception.
        }
        return $total;
    }
    
    /**
     * @param SearchStore $search
     * @param $page
     * @param $limit
     * @return int|mixed|string
     */
    public function findSearchPaginated(SearchStore $search, $page, $limit)
    {
        $query = $this
            ->createQueryBuilder('c')
            ->select('c');
        
        if (!empty($search->q)) {
            $query = $query
                ->andWhere('c.name LIKE :q')
                
                ->setParameter('q', "%{$search->q}%");
        }

        if (isset($search->status)) {
            $query = $query
                ->andWhere('c.status = :status')
                ->setParameter('status', $search->status);
        }
        $query = $query
            ->orderBy('c.id', 'DESC')
            ->setFirstResult(($page * $limit) - $limit)
            ->setMaxResults($limit);
        
        return $query->getQuery()->getResult();
    }

}
